import { configureStore, createSlice } from "@reduxjs/toolkit";
import { cloneDeep } from "lodash";

const searchData = createSlice({
  name: "suggestionList",
  initialState: {
    data: [],
    searchInputText: "",
    suggestionBoxClickFlag: false,
    showSearchResult: false,
  },
  reducers: {
    addsuggestions(state, actions) {
      state.data = actions.payload.list;
    },
    searchInputHandler(state, actions) {
      state.searchInputText = actions.payload.input;
    },
    suggestionBoxClickFlagHandler(state, actions) {
      state.suggestionBoxClickFlag = actions.payload.flag;
    },
    showSearchResulthandler(state, actions) {
      state.showSearchResult = actions.payload.flag;
    },
  },
});
const cartInitialState = {
  data: [],
  totalAmount: 0,
  showCartModal: false,
  isDataPresent: false,
  cartList: [],
};
const cartSlice = createSlice({
  name: "cartState",
  initialState: cartInitialState,
  reducers: {
    addToCart(state, actions) {},
    getCartData(state, actions) {
      state.data = actions.payload.list;
    },
    showCart(state, actions) {
      state.showCartModal = true;
    },
    hideCart(state, actions) {
      state.showCartModal = false;
    },
    addItemToCartList(state, actions) {
      state.cartList.push({
        key: actions.payload.item.id,
        item: actions.payload.item,
      });
    },
    removeItemFromCartList(state, actions) {},
    decreaseCartListItemCount(state, actions) {
      let id;
      actions.payload.data.forEach((item, index) => {
        if (item.key === actions.payload.item.id) {
          id = index;
        }
      });
      state.cartList[id].item.count--;
      if (state.cartList[id].item.count <= 0) {
        state.cartList.splice(id, 1);
      }
    },
    increaseCartListItemCount(state, actions) {
      let id;
      actions.payload.data.forEach((item, index) => {
        if (item.key === actions.payload.item.id) {
          id = index;
        }
      });
      state.cartList[id].item.count++;
    },
    addItemsToStore(state, actions) {
      state.data = actions.payload.data;
      if (state.data.length > 0) {
        state.isDataPresent = true;
      }
    },
    updateItemInStore(state, actions) {
      if (actions.payload.operation === "INCREMENT")
        state.data[+actions.payload.item.id].count++;
      else if (actions.payload.operation === "DECREMENT")
        state.data[+actions.payload.item.id].count--;
    },
    setTotalamount(state, actions) {
      if (actions.payload.operation === "ADD") {
        state.totalAmount +=
          actions.payload.list.count * actions.payload.list.price;
      } else {
        state.totalAmount -=
          actions.payload.list.count * actions.payload.list.price;
      }
    },
  },
});

const store = configureStore({
  reducer: {
    suggestionList: searchData.reducer,
    cart: cartSlice.reducer,
  },
});
export const suggestionListActions = searchData.actions;
export const cartActions = cartSlice.actions;
export default store;
