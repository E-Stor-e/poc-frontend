import React, { useState } from "react";
import { useSelector } from "react-redux";
import "./App.css";
import { Route } from "react-router";
import SearchBar from "./components/Layout/SearchBar/SearchBar";
import SearchResult from "./components/Layout/SearchBar/SearchResult";
import NavBar from "./components/Layout/NavBar/NavBar";
import CategoryItems from "./components/UI/CategoryItems";
import StoreList from "./components/UI/StoreList";
import ItemList from "./components/UI/ItemList";
import Cart from "./components/UI/Cart";
function App() {
  const showCart = useSelector((state) => state.cart.showCartModal);
  return (
    <React.Fragment>
      <NavBar />
      <Route path="/home">
        {" "}
        <SearchBar />
      </Route>
      <Route path="/search">
        <SearchResult />
      </Route>
      <Route path="/home">
        <CategoryItems />
      </Route>
      <Route path="/store">
        <StoreList />
      </Route>
      <Route path="/items">
        <ItemList />
      </Route>
      {showCart && <Cart />}
    </React.Fragment>
  );
}

export default App;
