import React from "react";
import log from "../util/log";

const MockApi = (props) => {
  fetch("http://localhost:3000/data")
    .then((res) => res.json())
    .then(
      (result) => {
        log("result>>>", result);
      },
      (error) => {
        log("error>>>", error);
      }
    );
  return <h1>Hello</h1>;
};

export default MockApi;
