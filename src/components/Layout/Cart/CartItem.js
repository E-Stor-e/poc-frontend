import classes from "./CartItem.module.css";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { cartActions } from "../../../store/store";
const CartItem = (props) => {
  const dispatch = useDispatch();
  const cartList = useSelector((state) => state.cart.cartList);
  const increaseCountHandler = (event) => {
    dispatch(
      cartActions.updateItemInStore({
        item: event,
        operation: "INCREMENT",
      })
    );
    dispatch(
      cartActions.increaseCartListItemCount({
        item: event,
        data: cartList,
      })
    );
  };
  const decreaseCountHandler = (event) => {
    dispatch(
      cartActions.updateItemInStore({
        item: event,
        operation: "DECREMENT",
      })
    );
    dispatch(
      cartActions.decreaseCartListItemCount({
        item: event,
        data: cartList,
      })
    );
  };
  return (
    <React.Fragment>
      {cartList &&
        cartList.map(({ key, item }) => {
          return (
            <li key={item.id} id={item.id} className={classes["cart-item"]}>
              <div>
                <h2>{item.name}</h2>
                <div className={classes.summary}>
                  <span className={classes.price}>{item.price}</span>
                  <span className={classes.amount}>x {item.count}&nbsp;=</span>
                  <span className={classes.price}>
                    {item.count * item.price}
                  </span>
                </div>
              </div>
              <div className={classes.actions}>
                <button onClick={decreaseCountHandler.bind(null, item)}>
                  −
                </button>
                <button onClick={increaseCountHandler.bind(null, item)}>
                  +
                </button>
              </div>
            </li>
          );
        })}
    </React.Fragment>
  );
};

export default CartItem;
