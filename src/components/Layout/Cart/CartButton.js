import React from "react";
import { BiCart } from "react-icons/bi";
import styles from "./CartButton.module.css";
import { useSelector, useDispatch } from "react-redux";
import { cartActions } from "../../../store/store";
import { Badge } from "react-bootstrap";
const CartButton = (props) => {
  const dispatch = useDispatch();
  const cartList = useSelector((state) => state.cart.cartList);
  const cartHandler = () => {
    dispatch(cartActions.showCart());
  };
  return (
    <React.Fragment>
      <div className="d-flex align-items-center flex-column">
        <Badge pill variant="primary">
          {cartList?.length}
        </Badge>{" "}
        <BiCart
          onClick={cartHandler}
          className={styles["cart-icon"]}
          color="red"
        />
      </div>
    </React.Fragment>
  );
};

export default CartButton;
