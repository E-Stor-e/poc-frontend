import React from 'react';
import { Container, Navbar } from 'react-bootstrap';
import classes from './NavBar.module.css'
import CartButton from '../Cart/CartButton'

const NavBar = () => {

  

    return (
        <React.Fragment>
             <Navbar  bg="dark" variant="dark">
    <Container>
      <Navbar.Brand className={classes.navbarbrand} href="#home">
      E-Store
      </Navbar.Brand>
    </Container>
     <CartButton />
  </Navbar>
        </React.Fragment>
    );
};

export default NavBar;