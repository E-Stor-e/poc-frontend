import React, { useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import SearchBarInput from '../../UI/SearchBarInput'
import SearchBarSuggestions from '../../UI/SearchBarSuggestions'
import classes from './SearchBar.module.css';
const SearchBar = () => {
  const [searchInput, setSearchInput] = useState('')
  const [showInputSuggestion, setShowInputSuggestion] = useState(false)

  const onchange = (data) => {
    setShowInputSuggestion(data.flag)
    setSearchInput(data.value)
  }
  return (
    <Container fluid>
      <Row>
        <Col />
        <Col xs={6}>
          <Col />
          <div className={classes.searchBar}>
          <SearchBarInput
            onchange={(e) => {
              onchange(e)
            }}
          />
          <div className={classes.suggestionList}><SearchBarSuggestions
            searchInput={searchInput}
            showInputSuggestion={showInputSuggestion}
          />
          </div>
          </div>
        </Col>
        <Col />
      </Row>
    </Container>
  )
}

export default SearchBar
