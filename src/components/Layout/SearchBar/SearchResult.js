import React, { useEffect, useState } from "react";
import {
  Card,
  Col,
  Container,
  Image,
  ListGroup,
  Row,
  Tabs,
} from "react-bootstrap";
import { Tab } from "bootstrap";
import { useDispatch, useSelector } from "react-redux";
import useHttp from "../../../hooks/use-http";
import classes from "./SearchResult.module.css";
import { suggestionListActions } from "../../../store/store";

const SearchResult = () => {
  const dispatch= useDispatch();
  const suggestionListFlag = useSelector(
    (state) => state.suggestionList.suggestionBoxClickFlag
  );
  const showSearchResult = useSelector(
    (state) => state.suggestionList.showSearchResult
  );

  const { sendRequest: fetchSearchResult } = useHttp();
  const [storelist, setStoreList] = useState([]);
  const [itemlist, setItemList] = useState([]);

  const transfomSearchResult = (list) => {
    dispatch(
      suggestionListActions.showSearchResulthandler({ flag: true })
    );
    const store = list.filter((items) => items.category === "Store");
    const item = list.filter((items) => items.category === "Item");
    const storeList = store.map((items) => (
      <Card key={item.id} style={{ width: "30.5%", margin: "10px" }}>
        <Card.Body>
          <Image
            style={{ width: "100%" }}
            src={
              require(`../../../assets/storeImg/${items.imageName}.jpg`).default
            }
          />
          <Card.Title>{items.name}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            {" "}
            Best In Town
          </Card.Subtitle>
        </Card.Body>
      </Card>
    ));
    const itemList = item.map((items) => (
      <ListGroup.Item key={items.id}>
        <div>{items.name}</div>
      </ListGroup.Item> 
    ));
    setStoreList(storeList);
    setItemList(itemList);
  };

  useEffect(() => {
    if (!suggestionListFlag) {
      fetchSearchResult(
        { url: "http://localhost:3002/data" },
        transfomSearchResult
      );
    }
  }, [fetchSearchResult, suggestionListFlag]);

  return (

  <Container fluid>
       {showSearchResult && 
      <Row>
        <Col />
        <Col xs={6} className={classes.marginColumn}>
          <Tabs
            className="mb-3"
            defaultActiveKey="Store"
            id="uncontrolled-tab-example"
            transition={false}
          >
            <Tab eventKey="Store" title="Store">
              <Row> {storelist}</Row>
            </Tab>
            <Tab eventKey="Item" title="Item">
              <ListGroup>{itemlist}</ListGroup>
            </Tab>
          </Tabs>
        </Col>
        <Col />
      </Row>
    }
    </Container>
  );
};

export default SearchResult;
