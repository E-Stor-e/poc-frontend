import React from "react";
import { Card, Col, Container, Row, Badge } from "react-bootstrap";
import styles from "./CardItem.module.css";
import { BsForward } from "react-icons/bs";
const CardItem = (props) => {
  return (
    <Container fluid>
      <Row>
        {props.data.data &&
          props.data.data.map((category) => {
            return (
              <Col sm={4}>
                <Card
                  className={styles["shop-category"]}
                  key={category.id}
                  id={category.id}
                >
                  <Card.Img
                    variant="bottom"
                    src={category.imgUrl}
                    className={styles["category-img"]}
                  />
                  <Card.Body className={styles["card-body"]}>
                    <Card.Text>
                      <Badge bg="primary">
                        {category.name} ({category.count})
                      </Badge>
                    </Card.Text>

                    <BsForward className={styles["forward"]} />
                  </Card.Body>
                </Card>
              </Col>
            );
          })}
      </Row>
    </Container>
  );
};

export default CardItem;
