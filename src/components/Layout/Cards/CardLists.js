import CardItem from "./CardItem";
import React from "react";
import styles from "./CardLists.module.css";
import { Container } from "react-bootstrap";
const CardLists = (props) => {
  return (
    <React.Fragment>
      <Container fluid className={styles["category-list"]}>
        <section>
          <CardItem data={props.categories} />
        </section>
      </Container>
    </React.Fragment>
  );
};

export default CardLists;
