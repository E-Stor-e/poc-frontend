import React from "react";
import { Button } from "react-bootstrap";
import Modal from "./Modal";
import { cartActions } from "../../store/store";
import { useDispatch, useSelector } from "react-redux";
import styles from "./Cart.module.css";
import CartItem from "../Layout/Cart/CartItem";
const Cart = (props) => {
  const dispatch = useDispatch();
  const cartList = useSelector((state) => state.cart.cartList);
  let totalAmount = 0;
  cartList.forEach((item, index) => {
    totalAmount += item.item.count * item.item.price;
  });

  const cartHandler = () => {
    dispatch(cartActions.hideCart());
  };

  return (
    <React.Fragment>
      <Modal className={styles["modal-height"]}>
        <CartItem />
        <div className={styles.total}>
          <span>Total amount</span>
          <span>{totalAmount}</span>
        </div>
        <div className={styles.actions}>
          <Button variant="light" onClick={cartHandler}>
            Close
          </Button>
          <Button variant="success">Order</Button>{" "}
        </div>
      </Modal>
    </React.Fragment>
  );
};

export default Cart;
