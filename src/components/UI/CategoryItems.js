import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import CardLists from "../../components/Layout/Cards/CardLists";
import useHttp from "../../hooks/use-http";
const CategoryItems = (props) => {
  const [categoryState, setCategoryState] = useState({});
  const { sendRequest: fetchCategoryList } = useHttp();
  const transformSuggestionList = (res) => {
    setCategoryState(res);
  };

  useEffect(() => {
    fetchCategoryList(
      { url: "http://localhost:3003/categoryList.json" },
      transformSuggestionList
    );
  }, [fetchCategoryList]);
  return (
    <React.Fragment>
      {categoryState && (
        <Link to="/store">
          <CardLists categories={categoryState} />
        </Link>
      )}
    </React.Fragment>
  );
};

export default CategoryItems;
