import React from "react";
import { Container, Row, CardDeck, Card, Col, Button } from "react-bootstrap";
import styles from "./Items.module.css";
import classes from "../../App.module.css";
import { BiMinusCircle, BiPlusCircle } from "react-icons/bi";
import { useDispatch, useSelector } from "react-redux";
import { cartActions } from "../../store/store";
const Items = (props) => {
  const dispatch = useDispatch();
  const getDataFromStore = useSelector((state) => state.cart);
  let itemsList = [];
  if (getDataFromStore.isDataPresent) {
    itemsList = getDataFromStore.data;
  } else {
    itemsList = props.items.data;
  }
  const increaseItemCountHandler = (event) => {
    dispatch(
      cartActions.updateItemInStore({
        item: event,
        operation: "INCREMENT",
      })
    );
  };
  const decreaseItemCountHandler = (event) => {
    dispatch(
      cartActions.updateItemInStore({ item: event, operation: "DECREMENT" })
    );
  };
  const addItemToCartHandler = (event) => {
    dispatch(cartActions.addItemToCartList({ item: itemsList[event.id] }));
  };
  return (
    <React.Fragment>
      <Container
        fluid
        className={`${styles["item-list"]} ${classes["font-size-18"]}`}
      >
        <section>
          <Row>
            {itemsList &&
              itemsList.map((item) => {
                return (
                  <Col sm={3}>
                    <Card
                      className={styles["shop-item"]}
                      key={item.id}
                      id={item.id}
                    >
                      <Card.Header>{item.name}</Card.Header>
                      <Card.Img
                        variant="top"
                        src={item.imgUrl}
                        className={styles["item-img"]}
                      />
                      <Card.Body className={styles["card-body"]}>
                        <Card.Text className={styles["item-body"]}>
                          This is a wider card with supporting text below as a
                          natural lead-in to additional content. This content is
                          a little bit longer.
                        </Card.Text>
                        <div className={styles["flex-container"]}>
                          <div>
                            <BiPlusCircle
                              onClick={increaseItemCountHandler.bind(
                                null,
                                item
                              )}
                              key={item.id}
                              className={styles["size"]}
                            />
                            &nbsp;{item.count}&nbsp;
                            <BiMinusCircle
                              id={item.id}
                              onClick={decreaseItemCountHandler.bind(
                                null,
                                item
                              )}
                              className={styles["size"]}
                            />
                          </div>
                          <div className={classes["font-size-18"]}>
                            {item.price}
                          </div>
                        </div>
                        <Button
                          variant="success"
                          className={`${classes["font-size-14"]} ${styles["button"]}`}
                          onClick={addItemToCartHandler.bind(null, item)}
                          disabled={item.count <= 0}
                        >
                          Add to Cart
                        </Button>{" "}
                      </Card.Body>
                    </Card>
                  </Col>
                );
              })}
          </Row>
        </section>
      </Container>
    </React.Fragment>
  );
};

export default Items;
