import React, { useRef } from "react";
import { FormControl, InputGroup } from "react-bootstrap";
import { suggestionListActions } from "../../store/store";
import { useDispatch } from "react-redux";
import Button from "react-bootstrap/Button";
import classes from "./SearchBarInput.module.css";
import { useHistory } from "react-router";
const SearchBarInput = (props) => {
  const history = useHistory();
  const searchInputValue = useRef();
  const dispatch = useDispatch();
  const inputSuggestionHandler = (event) => {
    if (event.target.value === "") {
      props.onchange({ value: "", flag: false });
      dispatch(
        suggestionListActions.suggestionBoxClickFlagHandler({ flag: false })
      );
    } else {
      props.onchange({ value: event.target.value, flag: true });
      dispatch(
        suggestionListActions.suggestionBoxClickFlagHandler({ flag: true })
      );
    }
    dispatch(
      suggestionListActions.searchInputHandler({ input: event.target.value })
    );
  };
  const buttonSuggestionHandler = () => {
    props.onchange({ value: "", flag: false });
    searchInputValue.current.value = "";
    dispatch(suggestionListActions.searchInputHandler({ input: "" }));
    dispatch(
      suggestionListActions.suggestionBoxClickFlagHandler({ flag: false })
    );
    dispatch(suggestionListActions.showSearchResulthandler({ flag: false }));
    history.push("/search");
  };

  return (
    <>
      <InputGroup>
        <FormControl
          aria-label="Type Something..."
          aria-describedby="basic-addon2"
          onChange={inputSuggestionHandler}
          placeholder="Type Something..."
          ref={searchInputValue}
          className={`${classes.searchInput} ${classes.borderRightNone}`}
        />
        <InputGroup.Append>
          <Button
            onClick={buttonSuggestionHandler}
            variant="outline-secondary"
            className={classes.borderButton}
          >
            Clear
          </Button>
        </InputGroup.Append>
      </InputGroup>
    </>
  );
};

export default SearchBarInput;
