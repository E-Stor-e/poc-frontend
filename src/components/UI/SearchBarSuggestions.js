import React, { useEffect, useState } from "react";
import { Card, ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import { suggestionListActions } from "../../store/store";
import { useDispatch, useSelector } from "react-redux";
import useHttp from "../../hooks/use-http";
import classes from "./SearchBarSuggestions.module.css";

const SearchBarSuggestions = (props) => {
  const dispatch = useDispatch();

  const { searchInput } = props;
  const [suggestionList, setsuggestionList] = useState([]);
  const suggestionListFlag = useSelector(
    (state) => state.suggestionList.suggestionBoxClickFlag
  );

  const searchInputText = useSelector(
    (state) => state.suggestionList.searchInputText
  );
  const { sendRequest: fetchSuggestionList } = useHttp();

  const transformSuggestionList = (list) => {
    const hideSuggestionBox = () => {
      dispatch(
        suggestionListActions.suggestionBoxClickFlagHandler({ flag: false })
      );
      dispatch(suggestionListActions.showSearchResulthandler({ flag: true }));
    };

    const queryUrl = "/search?q=" + searchInputText;
    const transformedlist = list.map((item) => (
      <Link onClick={hideSuggestionBox} to={queryUrl} key={item.id}>
        {" "}
        <ListGroup.Item className={classes.borderTop}>
          <div className={classes.listMargin}>
            <div>{item.name}</div>
            <div>{item.category}</div>
          </div>
        </ListGroup.Item>
      </Link>
    ));

    setsuggestionList(transformedlist);
  };

  useEffect(() => {
    if (suggestionListFlag) {
      const identifier = setTimeout(() => {
        fetchSuggestionList(
          { url: "http://localhost:3001/data" },
          transformSuggestionList
        );
      }, 1000);

      return () => {
        clearTimeout(identifier);
      };
    }
  }, [searchInput, fetchSuggestionList, suggestionListFlag]);

  return (
    <>
      {suggestionListFlag && (
        <Card style={{ border: "none" }}>
          <ListGroup style={{ border: "none" }} variant="flush">
            {suggestionList}
          </ListGroup>
        </Card>
      )}
    </>
  );
};

export default SearchBarSuggestions;
