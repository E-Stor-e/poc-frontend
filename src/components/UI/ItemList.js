import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import CardLists from "../../components/Layout/Cards/CardLists";
import useHttp from "../../hooks/use-http";
import Items from "../UI/Items";
import { useDispatch, useSelector } from "react-redux";
import { cartActions } from "../../store/store";
import { cloneDeep } from "lodash";
const ItemList = () => {
  const dispatch = useDispatch();
  const [itemState, setItemState] = useState({});
  const { sendRequest: fetchItemsList } = useHttp();
  const isDataPresent = useSelector((state) => state.cart.isDataPresent);
  const transformList = (res) => {
    dispatch(cartActions.addItemsToStore({ data: res.data }));
    setItemState(res);
  };
  useEffect(() => {
    fetchItemsList(
      { url: "http://localhost:3003/ItemList.json" },
      transformList
    );
  }, [fetchItemsList]);
  return (
    <React.Fragment>
      <Items items={itemState} />
    </React.Fragment>
  );
};

export default ItemList;
