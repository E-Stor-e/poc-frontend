import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import CardLists from "../../components/Layout/Cards/CardLists";
import useHttp from "../../hooks/use-http";
import { useParams } from "react-router-dom";
const StoreList = () => {
  const [storeState, setStoreState] = useState({});
  const { sendRequest: fetchStoreList } = useHttp();
  const transformList = (res) => {
    setStoreState(res);
  };
  useEffect(() => {
    fetchStoreList(
      { url: "http://localhost:3003/storeList.json" },
      transformList
    );
  }, [fetchStoreList]);
  return (
    <React.Fragment>
      {storeState && (
        <Link to="/items">
          <CardLists categories={storeState} />
        </Link>
      )}
    </React.Fragment>
  );
};

export default StoreList;
